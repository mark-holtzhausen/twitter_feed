var UserException = function () {
};
UserException.prototype = new Error();
UserException.prototype.constructor = Error;


var TweetException = function () {
};
TweetException.prototype = new Error();
TweetException.prototype.constructor = Error;


var UserListException = function () {
};
UserListException.prototype = new Error();
UserListException.prototype.constructor = Error;


module.exports = {UserException: UserException, UserListException: UserListException, TweetException: TweetException};
