/**
 * Create a UserList
 * @constructor UserList
 * @method UserList
 * @return
 */
var UserList = function () {
    var UserList = this; // Disambiguate context

    var User = require('./user');

    var users = {};

    /**
     * Add a user to this userList
     * @method addUser
     * @param {User} user
     * @return UserList
     */
    UserList.addUser = function (user) {

        if (!(user instanceof User)) throw new Error("UserList.addUser(user):: requires user to be an instance of User");

        var userName = user.getName();

        if (UserList.userExists(userName))  throw new Error("UserList.addUser(user):: can only add users that don\'t already exist on the userList.");
        user.setUserList(UserList);
        users[userName] = user;

        return UserList;
    };

    /**
     * Does the provided user exist on the userList
     * @method userExists
     * @param {String} name
     * @return {Boolean}
     */
    UserList.userExists = function (name) {
        return UserList.getUserList().indexOf(name) >= 0;
    };


    /**
     * Get the user by the provided name
     * @throws {Error} User Doesn't Exist
     * @method getUser
     * @param {String} name
     * @return {User}
     */
    UserList.getUser = function (name) {
        if (!UserList.userExists(name)) throw new Error("UserList.getUser(name):: could not find user " + name);
        return users[name];
    };

    /**
     * Get User List
     * @method getUserList
     * @return {Array.<String>}
     */
    UserList.getUserList = function () {
        return Object.keys(users);
    };

    /**
     * Get All Users on the userList
     * @method getUsers
     * @return {Object} named users
     */
    UserList.getUsers = function () {
        return users;
    };


};

module.exports = UserList;