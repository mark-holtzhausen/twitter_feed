

/**
 * Creates a new User
 * @constructor
 * @return
 * @method User
 * @param userName
 * @param {Array.<User>} followersList
 * @return 
 */
var User = function (userName, followersList) {
    var User = this; //disambiguate the context;

    var UserList=require('./userList');
    var Exception=require('./exceptions');

    var Tweet = require('./tweet');
    var followers = {};
    var name;
    var tweets=[];
    name = userName;


    /**
     * Initializes the object
     * @method init
     * @param {Array} followersList
     * @return 
     */
    var init = function init(followersList) {
        if (followersList)User.addFollowers(followersList);
    };

    /**
     * Get the name of the user
     * @method getName
     * @return {String} name
     */
    User.getName = function () {
        return name;
    };

    /**
     * Get the followers of the user
     * @method getFollowers
     * @return {Object.<User>} followers
     */
    User.getFollowers = function () {
        return followers;
    };

    /**
     * Does the user have the provided follower
     * @method hasFollower
     * @param {String} name
     * @return {Boolean}
     */
    User.hasFollower = function (name) {
        return User.getFollowers().hasOwnProperty(name);
    };

    /**
     * Add a follower for this user
     * @method addFollowers
     * @param {Array.<User>} followerList
     * @return 
     */
    User.addFollowers = function (followerList) {
        if(!followerList) throw new Exception.UserException("User.addFollowers(followerList) :: followerList is a required parameter.");
        if(!Array.isArray(followerList)) throw new Exception.UserException("User.addFollowers(followerList) :: followerList is an User[]");

        followerList.map(function(follower){
            if(!follower instanceof User.constructor) throw new Exception.UserException("User.addFollowers(followerList) :: followerList is a list of Users. (User[])");
            var followerName = follower.getName();
            if (!User.hasFollower(followerName)) {
                followers[followerName]=follower;
            }
        });
    };


    /**
     * set this user's UserList
     * @method setUserList
     * @param {UserList} userList
     * @return {User}
     */
    //TODO: Can I get rid of this?
    User.setUserList=function(userList){
        if (!(userList instanceof UserList)) throw new Exception.UserException("User.setUserList(userList) :: requires userList to be of type UserList.");
        User.userList=userList;
        return User;
    };

    /**
     * Send a tweet
     * @method sendTweet
     * @param {String} message
     * @return 
     */
    User.sendTweet=function(message){
        var tweet=new Tweet(User,message);
        return tweet;
    };

    /**
     * Receive a tweet
     * @method receiveTweet
     * @param {Tweet} tweet
     * @return 
     */
    User.receiveTweet=function(tweet){
        if (!(tweet instanceof Tweet)) throw new Error("User.receiveTweet(tweet):: requires tweet to be an instance of Tweet");
        tweets.push(tweet);
    };

    /**
     * Returns all the received tweets for this user
     * @method getTweets
     * @return {Array}
     */
    User.getTweets=function(){
        return tweets;
    };

    init(followersList);

};

module.exports = User;