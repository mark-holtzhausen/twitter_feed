/**
 * Description
 * @constructor Tweet
 * @method Tweet
 * @param {User} user
 * @param {String} message
 * @return 
 */
var Tweet = function (user, message) {
    var Tweet = this; // Disambiguate context

    var User = require("./user");
    var Exception = require("./exceptions");


    /**
     * Description
     * @method init
     * @param {User} user
     * @param {String} message
     * @return 
     */
    var init = function (user,message) {
        if (!(user instanceof User)) throw new Exception.TweetException("Tweet(user,message) :: requires user to be of type User");
        Tweet.user = user;

        Tweet.message = ''+message;


        Tweet.user.receiveTweet(Tweet);

        var followers=Tweet.user.getFollowers();
        for(var name in followers){
            followers[name].receiveTweet(Tweet);
        }
   };

    init(user, message);
};


module.exports = Tweet;