var Stream = require('stream');
function getStreamLineReader(){
    var streamLineReader = new Stream.Transform({objectMode: true});

    /**
     * Split incoming data into lines
     * @param chunk
     * @param encoding
     * @param done
     * @private
     */
    streamLineReader._transform = function (chunk, encoding, done) {
        var chunkData = chunk.toString();
        if (this._buffer) chunkData = this._buffer + chunkData;

        var lines = chunkData.split('\n'); // split by line
        this._buffer = lines.splice(lines.length - 1, 1)[0];

        lines.forEach(this.push.bind(this));
        done()
    };

    /**
     * Flush the remaining data on the buffer
     * @param done
     * @private
     */
    streamLineReader._flush = function (done) {
        if (this._buffer) this.push(this._buffer);
        this._buffer = null
        done()
    };
    return streamLineReader;
};

module.exports = getStreamLineReader;