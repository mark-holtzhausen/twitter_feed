var UserList = require('./userList');
var User = require('./user');
var fs = require('fs');
var colors=require('colors');

/**
 * The Application object
 * @constructor App
 * @method App
 * @param {} userFile
 * @param {} tweetFile
 * @return
 */
var App = function (userFile, tweetFile) {
    var App = this; // Disambiguate context
    var userList = new UserList();
    var userErrors=[],tweetErrors=[];

    /**
     * Application main
     * @method init
     * @param {String} userFile
     * @param {String} tweetFile
     * @return
     */
    App.init = function (userFile, tweetFile) {
        var fileFault = false;

        if (!fs.existsSync(userFile)) {
            console.error('User File: ' + userFile + ' does not exist.');
            fileFault = true;
        }

        if (!fs.existsSync(tweetFile)) {
            console.error('Tweet File: ' + tweetFile + ' does not exist.');
            fileFault = true;
        }

        if (fileFault) {
            process.exit(1);
        }

        App.createUserRelationships(userFile, function () {
            App.importTweets(tweetFile, function () {
                App.reportUserTweets();
            });
        });
    };

    /**
     * Create User Relationships from the userFile
     * @method createUserRelationships
     * @param {String} userFile
     * @param {Function} done
     * @return
     */
    App.createUserRelationships = function (userFile, done) {
        var lineReader = require('./streamLineReader')();

        var rxExtract = new RegExp(/^ *((?:[A-Za-z0-9])*)(?:\s+follows\s+)((?:[A-Za-z0-9\ ,]*[A-Za-z0-9]))*$/im);

        /**
         * Parse a line
         * @method parseLine
         * @param {String} line
         * @return
         */
        var parseLine = function (line) {
            line=line.replace('\r','');
            var match = line.match(rxExtract);
            if (match == null) {
                userErrors.push('Could not parse user line: ('+line+')');
            } else {
                var followerName = match[1].trim();
                if (!userList.userExists(followerName)) {
                    userList.addUser(new User(followerName));
                }
                var follower = userList.getUser(followerName);

                match[2].split(',').map(function (userName) {
                    userName = userName.trim();
                    if (!userList.userExists(userName)) {
                        userList.addUser(new User(userName));
                    }
                    userList.getUser(userName).addFollowers([follower]);
                });
            }
        };

        var userData = fs.createReadStream(userFile);
        userData.pipe(lineReader);
        lineReader.on('readable', function () {
            var line;
            while (line = lineReader.read()) {
                parseLine(line);
            }
        });

        lineReader.on('end', function () {
            var users = userList.getUsers();
            for (var userName in users) {
                var user = users[userName];
            }
            if (typeof done == 'function')done();
        });

        lineReader.on('error', function () {
            console.error('An Error Occurred', arguments);
        });


    };

    /**
     * Import Tweets
     * @method importTweets
     * @param {String} tweetFile
     * @param {function} done
     * @return
     */
    App.importTweets = function (tweetFile, done) {
        var lineReader = require('./streamLineReader')();

        var rxExtract = new RegExp(/^([A-z][A-Za-z0-9]*)\s*\>\s*(.*)$/im);
        /**
         * Description
         * @method parseLine
         * @param {} line
         * @return
         */
        var parseLine = function (line) {
            line=line.replace('\r','');
            var match = line.match(rxExtract);
            if (match == null) {
                tweetErrors.push('Could not parse tweet line: ('+line+')');
            } else {
                var userName = match[1].trim();
                var message = match[2].trim();

                if (!userList.userExists(userName)) {
                    userList.addUser(new User(userName));
                }

                var tweet = userList.getUser(userName).sendTweet(message);
            }
        };

        var tweetData = fs.createReadStream(tweetFile);
        tweetData.pipe(lineReader);
        lineReader.on('readable', function () {
            var line;
            while (line = lineReader.read()) {
                parseLine(line);
            }
        });

        lineReader.on('end', done);

        lineReader.on('error', function () {
            console.error('An Error Occurred', arguments);
        });
    };

    /**
     * Report User Tweets
     * @method reportUserTweets
     * @return
     */
    App.reportUserTweets = function () {
        var users = userList.getUserList().sort();
        users.forEach(function (userName) {
            var user = userList.getUser(userName);
            var tweets = user.getTweets().map(function (tweet) {
                return '@' + tweet.user.getName() + ': ' + tweet.message;
            });
            console.log(userName + '\n\t' + tweets.join('\n\t'));
        });
        if(userErrors.length>0){
            console.error('\n','UserFile Parse Errors:'.red.bold);
            userErrors.forEach(function(err){
                console.error(colors.red('\t* '+err));
            })
        }

        if(tweetErrors.length>0){
            console.error('\n','TweetFile Parse Errors:'.red.bold);
            tweetErrors.forEach(function(err){
                console.error(colors.red('\t* '+err));
            })
        }

    };


    App.init(userFile, tweetFile);
};

module.exports = App;