## Requirements
 - To run this app, ensure **Node V0.12 >** is installed on your environment
 - The test suite requires the installation of Jasmine, the testing framework. To install Jasmine, `sudo npm install -g jasmine`

## How to Run

    > git clone https://nemesarial@bitbucket.org/nemesarial/twitter_feed.git
    > cd twitter_feed
    > npm install
    > node tweet.js spec/user.txt spec/tweet.txt
    > # Some Error Files:
    > node tweet.js spec/broken_user.txt spec/broken_tweet.txt
    > # To hide any error output:
    > node tweet.js spec/broken_user.txt spec/broken_tweet.txt 2>/dev/null 
    
## How to Test

    > npm test
    > #or
    > jasmine
    
## Language Choice
I chose to write this in JavaScript because it shares many features with Scala which I understand is your primary backend language.  
I am also very familiar and comfortable with JavaScript and thought it would be a good demonstration of how to use it 
for OOP work.

## Assumptions
Here are some assumptions that I made regarding this excercise:  

 - That the input files shouldn't exceed **1MB** each in size.
 - That code readability is more useful in this excercise than code optimization.