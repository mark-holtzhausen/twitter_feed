var UserList = require('../lib/userList');
var User = require('../lib/user');
var Tweet = require('../lib/tweet');


describe("User", function () {

    it("Should be able to create a User", function () {
        var userTestName = 'TheUserName';

        var user = new User(userTestName);

        expect(user.getName()).toEqual(userTestName);
    });

    it("Should be able to add followers from the constructor", function () {
        var followers = [new User('follower1'), new User('follower2'), new User('follower3')];

        var user = new User('testUser', followers);
        followers.forEach(function (follower) {
            expect(user.hasFollower(follower.getName())).toBeTruthy();
        });
    });

    it("Should be able to add followers after construction", function () {
        var followers = [new User('follower1'), new User('follower2'), new User('follower3')];

        var user = new User('testUser');
        user.addFollowers(followers);
        followers.forEach(function (follower) {
            expect(user.hasFollower(follower.getName())).toBeTruthy();
        });

    });

    it("UserList should set user userList on addUser", function(){
        var userList=new UserList();
        var user=new User("testUser");

        userList.addUser(user);
        expect(user.userList).toEqual(userList);
    });

    it("Should be keep track of received tweets",function(){
        var user=new User("testUser");
        var tweet=new Tweet(user,'tweet_message');
        expect(user.getTweets()[0]).toEqual(tweet);
    });

});



describe("UserList", function () {

    it("Should only allow adding users of type User", function () {
        var userList = new UserList();

        var fnErr = function () {
            userList.addUser('user');
        };
        var fnNoErr = function () {
            userList.addUser(new User('testuser'));
        };

        expect(fnErr).toThrow();
        expect(fnNoErr).not.toThrow();
    });

    it("Should be able to tell whether a user already exists on the userList", function () {
        var userList = new UserList();
        var user = new User('testUser');
        userList.addUser(user);

        expect(userList.userExists(user.getName())).toBeTruthy();
    });

    it("Should prevent you from adding a user that already exists", function () {
        var userList = new UserList();
        var user = new User('testUser');

        var fn = function () {
            userList.addUser(user)
        };

        expect(fn).not.toThrow();
        expect(fn).toThrow();
    });


    it("Should be able to return a user when requested by name", function () {
        var userList = new UserList();
        var user = new User('testUser');
        userList.addUser(user);

        expect(userList.getUser(user.getName())).toBe(user);
    });

    it("Should provide a list of user names on the userList", function () {
        var userList = new UserList();

        var user1 = new User('testUser');
        var user2 = new User('testUser2');

        userList.addUser(user1);
        userList.addUser(user2);

        var userList = userList.getUserList();

        expect(userList.indexOf(user1.getName())).toBeGreaterThan(-1);
        expect(userList.indexOf(user2.getName())).toBeGreaterThan(-1);
        expect(userList.length).toEqual(2);
    });

    it("Should provide a list of users on the userList", function () {
        var userList = new UserList();

        var user1 = new User('testUser');
        var user2 = new User('testUser2');

        userList.addUser(user1);
        userList.addUser(user2);

        var users = userList.getUsers();

        expect(users).toEqual({
            "testUser":user1,
            "testUser2":user2
        });
    });

});

describe("Tweet", function(){
    it("Should proagate itself", function(){
        var user=new User("testUser");
        var friend1=new User("testUser1");
        var friend2=new User("testUser2");
        user.addFollowers([friend1,friend2]);


        var tweet = user.sendTweet('tweet message');
        expect(user.getTweets()[0]).toBe(tweet);
        expect(friend1.getTweets()[0]).toBe(tweet);
        expect(friend2.getTweets()[0]).toBe(tweet);
    });
});