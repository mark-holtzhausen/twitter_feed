#!/usr/bin/node
var program = require('commander');

program
    .version('0.0.1')
    .arguments('<userFile> <tweetFile>')
    .action(function (userFile, tweetFile) {
        var app = new require('./lib/app')(userFile, tweetFile);
    })
    .parse(process.argv);

if (!process.argv.slice(2).length) {
    program.outputHelp();
}